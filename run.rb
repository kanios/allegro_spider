require 'open-uri'
require 'nokogiri'
require 'date'
require 'csv'
require './src/spider'
require './src/scraper'
require './src/csvgen'

id = 48597057
timer = Time.now
csv_path = './data/' + timer.to_s + '.csv'

for i in id - 250000..id + 100
  CsvGen.new(i.to_s, csv_path)
  puts i
end
