class Spider
  public def initialize(allegro_id)
    @id = allegro_id
  end

  public def get_data
    doc_ratings = Scraper.new(get_ratings_url).source

    return Array.new if doc_ratings == ''

    is_company = get_is_company(doc_ratings)

    return Array.new if !is_company

    login = get_login(doc_ratings)
    ratings = get_ratings(doc_ratings)

    doc_profile = Scraper.new(
        get_profile_url(login)
    ).source

    return Array.new if doc_profile == '' ||
        doc_profile.css('._342830a a').nil? ||
        doc_profile.css('._342830a a')[0].nil?

    doc_auctions = Scraper.new(
        doc_profile.css('._342830a a')[0]['href']
    ).source

    return Array.new if doc_auctions == ''

    script = doc_auctions.css('script').text

    email = get_email(script)
    phone = get_phone(script)
    company_data = get_company_data(doc_auctions)

    result = [ @id, login, ratings, email, phone, company_data ]

    result.each do |el|
      el.to_s
    end

    result
  end

  private def get_company_data(doc_auctions)
    begin
      doc_auctions
          .css('.d03ef94d')
          .css('._1f9594c7')
          .css('div')[0]
          .to_s.gsub(/<[^>]*>/ui,'')
    rescue
      'brak danych o firmie'
    end
  end

  private def get_phone(script)
    begin
      script[
          script.index('"phones":')..script.index('"phones":') + 30
      ].tr('^0-9', '')
    rescue
      'brak numeru telefonu'
    end
  end

  private def get_email(script)
    begin
      email_part = script[script.index('mailto:')..script.index('mailto:') + 30]
      email_part[7..email_part.index('"') - 1]
    rescue
      'brak adresu email'
    end
  end

  private def get_is_company(doc_ratings)
    doc_ratings.css('.user-info__company').any?
  end

  private def get_login(doc_ratings)
    begin
      doc_ratings
          .css('.user-info')
          .text.sub(' Użytkownik ', '').strip
    rescue
      'brak loginu'
    end
  end

  private def get_ratings(doc_ratings)
    begin
      doc_ratings
          .css('.ratings-tabs__all')
          .text.sub('Wszystkie (', '').chomp(')').to_i
    rescue
      'brak ocen'
    end
  end

  private def get_ratings_url
    'https://allegro.pl/uzytkownik/' + @id + '/oceny'
  end

  private def get_profile_url(login)
    'https://allegro.pl/uzytkownik/' + login +'?order=n'
  end
end
