class CsvGen

  def initialize(id, path)
    spider = Spider.new(id)
    data = spider.get_data

    CSV.open(path, 'ab') do |csv|
      csv << data if data != []
    end
  end

end
