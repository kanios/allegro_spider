class Scraper

  def initialize(url)
    begin
      html = open(url)
      @source = Nokogiri::HTML(html)
    rescue
      @source = ''
    end
  end

  attr_reader :source

end